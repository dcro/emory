###############################################################################
cmake_minimum_required(VERSION 3.8.0)
project(emory CXX)

set(CMAKE_CXX_STANDARD 20)

list (APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake)
include (nc)

include_directories(.)


###############################################################################
option (TESTS "enable unit testing" ON)
if (TESTS)
    enable_testing ()
endif ()


###############################################################################
add_subdirectory(cruft/util)
add_subdirectory(cruft/crypto)


###############################################################################
add_executable (emory
    emory.cpp
)


##-----------------------------------------------------------------------------
target_link_libraries(emory cruft-crypto cruft-util)
